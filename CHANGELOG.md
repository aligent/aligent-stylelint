# 1.4.0

- Add new plugin, aligent/no-partial-class-names

# 1.3.1

- fix NPM version on stylelint

# 1.3.0

- update stylelint to 12.x

# 1.2.0

- Add new plugin, aligent/no-unjustified-important

# 1.1.0

- Update package name

# 1.0.0

- Initial release

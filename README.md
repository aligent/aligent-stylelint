# Aligent Stylelint

Stylelint configuration used internally at Aligent Consulting

## Install

`npm i -D @aligent/stylelint-preset`

### `.stylelintrc` configuration file

Add a `.stylelintrc` file to the root of your project, with the following content

```
{
  "extends": "@aligent/stylelint-preset"
}
```

## Development

### Time Tracking

Use the code `ALG-119` and project `Aligent` in Toggl when working on this repository.
Ensure the name of this repo, as well as an adequate description of work being undertaken is also included

Examples

`ALG-119: AligentReactCheckout - Updating package.json dependencies`

`ALG-119: AligentReactCheckout - Issue #2 Fixing eslint rules`

*The "Issue #2" part of the time entry above refers to [Issues](https://bitbucket.org/aligent/aligent-react-checkout/issues?status=new&status=open) in this repository*

Examples of what we **don't** want to see in toggle

`ALG-119: AligentReactCheckout`

`ALG-119`

## Test

`npm test`

In the test output, there *will* be Stylelint warnings displayed when the linter is run over `text/index.scss`. This is the intended (for now) output. If there is a rule not reported that should be, than that is something that should be fixed.

## Deployment

1. Update changelog and commit
  - **Don't** change the version in `package.json`
2. Run `npm version <patch|minor|major>`
  - This will automatically update the version number in `package.json`
3. `git push --tags`
4. `npm publish`
 - Will need to ensure you have publishing permission

module.exports = {
    extends: 'stylelint-config-sass-guidelines',
    plugins: [
        'stylelint-scss',
        './plugins/aligent-no-unjustified-important-plugin',
        './plugins/aligent-no-partial-class-names-plugin'
    ],
    rules: {
        'color-hex-case': 'upper',
        'order/properties-alphabetical-order': null,
        'max-nesting-depth': 3,
        'color-hex-length': 'long',
        'scss/double-slash-comment-whitespace-inside': 'always',
        'selector-class-pattern':
            '^(?:(?:o|c|u|t|s|is|has|_|js|qa)-)?[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*(?:__[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*)?(?:--[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*)?(?:\\[.+\\])?$',
        "aligent/no-unjustified-important": true,
        "aligent/no-partial-class-names": true
    },
};

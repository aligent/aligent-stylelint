const stylelint = require('stylelint');

const ruleName = 'aligent/no-partial-class-names';
const messages = stylelint.utils.ruleMessages(ruleName, {
    expected() {
        return `No partial class names with interpolation`;
    }
});

/**
 * Disallowed selectors look like:
 *  &Class
 *  &-
 *  &_
 *
 * Allowed selectors look like
 *  &.disabled
 *  &:hover
 *  & > &
 *
 * Basically & can't be used to represent only part of a selector, it has to be a whole word
 */
module.exports = stylelint.createPlugin(ruleName, enabled => {
    if (!enabled) {
        return;
    }

    return (root, result) => {
        root.walkRules(rule => {
            const selector = rule.selector;
            const disallowedSelectorPattern = /&[a-zA-Z_-]/;
            const match = selector.match(disallowedSelectorPattern);
            if (match) {
                stylelint.utils.report({
                    result,
                    ruleName,
                    message: messages.expected(),
                    node: rule,
                    word: match[0]
                });
            }
        });
    };
});

module.exports.ruleName = ruleName;
module.exports.messages = messages;

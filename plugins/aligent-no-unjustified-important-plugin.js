const stylelint = require('stylelint');

const ruleName = 'aligent/no-unjustified-important';
const messages = stylelint.utils.ruleMessages(ruleName, {
    expected(value) {
        return `Justify use of !important`;
    }
});

module.exports = stylelint.createPlugin(ruleName, (enabled) => {
    if (!enabled) {
        return;
    }
    return (root, result) => {
        root.walkDecls((decl) => {
            if (decl.important) {
                // `!important` can only be used if there is a justifying comment
                const cssLine = decl.source.input.css.split('\n')[decl.source.start.line - 1];

                if (!cssLine.match('! ?important; *//') && !cssLine.match('! ?important; */\\*')) {
                    stylelint.utils.report({
                        result,
                        ruleName,
                        message: messages.expected(`${decl.prop}, ${decl.value}`),
                        node: decl,
                        word: 'important'
                    });
                }
            }
        });
    };
});

module.exports.ruleName = ruleName;
module.exports.messages = messages;

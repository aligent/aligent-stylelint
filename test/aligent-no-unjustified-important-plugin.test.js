const testRule = require('stylelint-test-rule-tape');
const noUnjustifiedImportant = require('../plugins/aligent-no-unjustified-important-plugin');

testRule(noUnjustifiedImportant.rule, {
    ruleName: noUnjustifiedImportant.ruleName,
    config: { preset: 'suit' },
    skipBasicChecks: true,

    accept: [
        { code: `/** @define Foo */ .Foo { background-color: #BADA55 !important; /* justifying comment! */ }` },
        { code: `/** @define Foo */ .Foo { background-color: #BADA55 ! important; /* justifying comment! */ }` },
        // A single line comment is also acceptable, however it's not possible to run that through the test,
        // as the test requires everything to be on a single line, including the opening/closing brackets of the selector block
    ],

    reject: [
        {
            code: `/** @define Foo */ .Foo { .background-color: #BADA55 !important;}`,
            message: `Justify use of !important (${noUnjustifiedImportant.ruleName})`,
            line: 1,
            column: 55,
        },
        {
            code: `/** @define Foo */ .Foo { .background-color: #BADA55 ! important;}`,
            message: `Justify use of !important (${noUnjustifiedImportant.ruleName})`,
            line: 1,
            column: 56,
        }
    ],
});
